import { Footer } from '../components/Footer'
import { Main } from '../components/FrontPage/Main'
import { Header } from '../components/Header'

const Root = () => {
	return (
		<>
			<Header />
			<Main />
			<Footer />
		</>
	)
}

export { Root }
