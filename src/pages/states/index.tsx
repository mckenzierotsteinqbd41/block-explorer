import { Header } from '../../components/Header'
import { StatesMain } from '../../components/States/Main'

export const PageStates = () => {
	return (
		<>
			<Header />
			<StatesMain />
		</>
	)
}
