import { CompletedSCRMain } from '../../components/CompletedSCR/Main'
import { Header } from '../../components/Header'

export const PageCompletedSCR = () => {
	return (
		<>
			<Header />
			<CompletedSCRMain />
		</>
	)
}
