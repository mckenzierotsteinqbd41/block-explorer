import { Link } from 'react-router-dom'
import { Footer } from '../../components/Footer'
import { Header } from '../../components/Header'
import './index.scss'

const Page404 = () => {
	return (
		<>
			<Header />
			<section className='page-404'>
				<div className='page-404-content'>
					<h1>404</h1>
					<p className='page-404-first'>Page not found</p>
					<p className='page-404-second'>
						Sorry, the page you are trying to access does not exist.
					</p>
					<Link to='/' replace>
						<button>
							<span>Back to home page</span>
						</button>
					</Link>
				</div>
			</section>
			<Footer />
		</>
	)
}

export { Page404 }
