import { useRouteError } from 'react-router-dom'
import { Page404 } from '../404'

const DefaultErrorPage = () => {
	return (
		<div id='error-page'>
			<h1>Oops!</h1>
			<p>Sorry, an unexpected error has occurred.</p>
		</div>
	)
}

const GlobalErrorPage = () => {
	const error = useRouteError() as { status: number }

	return (
		error.status === 404 ? <Page404 /> : <DefaultErrorPage />
	)
}

export { GlobalErrorPage }
