import './index.scss'

type Props = {
	text: string
	href: string
}

const Link1104 = ({ text, href }: Props) => {
	return (
		<a href={href} className='link-1104'>
			{text}
		</a>
	)
}

export { Link1104 }
