import cn from 'classnames'
import { Link } from 'react-router-dom'
import './index.scss'

export type ViewAllButtonProps = {
	text: string
	to: string
	className?: string
}

const ViewAllButton = ({ text, to, className }: ViewAllButtonProps) => {
	return (
		<Link
			to={to}
			style={{
				textDecoration: 'none',
			}}
		>
			<div className={cn('view-all-button', className)}>
				{text}
			</div>
		</Link>
	)
}

export { ViewAllButton }
