import { useSCRTotalAmount } from '../../../hooks/use-scr-total-amount'
import { Footer } from '../../Footer'
import { Skeleton } from '../../Skeleton'
import { CompletedSCRTable } from '../Table'
import './index.scss'

const title = 'State change request'
const description = 'Total Amount: '

const formatAmount = (d: number) => {
	return new Intl.NumberFormat('en-US').format(d)
}

export const CompletedSCRMain = () => {
	const { data } = useSCRTotalAmount()

	return (
		<main className='main-completed-scr'>
			<section className='main-completed-scr-bg'>
				<section className='main-completed-scr-topper'>
					<h1>{title}</h1>
					{data
						? <p>{`${description}${formatAmount(data)}`}</p>
						: <Skeleton width={143} height={14} rx={4} />}
				</section>
				{/* <section className="main-completed-scr-bg"></section> */}
				<CompletedSCRTable />
			</section>
			<Footer />
		</main>
	)
}
