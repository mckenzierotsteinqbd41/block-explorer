import { Link, useSearchParams } from 'react-router-dom'
import './index.scss'
import { memo, useEffect, useState } from 'react'
import { ReactComponent as ArrowRight } from '../../../assets/arror-right.svg'
import { useCompletedSCR } from '../../../hooks/use-completed-scr'
import { CompletedSCRStatus } from '../../../types/completed-scr'
import { truncate } from '../../../utils/truncate'
import { CompletedSCRStatusComp } from '../../CompletedSCRStatus'
import { NameDisplay } from '../../NameDisplay'
import { Paginator } from '../../Paginator'
import { Skeleton } from '../../Skeleton'

type RowProps = {
	hash: string
	status: CompletedSCRStatus
	stateNum: string
	name: string
	age: string
	requestType: string
	from: string
	to: string
	fee: string
}

const Header = () => {
	return (
		<div className='completed-scr-table_header'>
			<p>
				Hash
			</p>
			<p>
				Status
			</p>
			<p>
				State Num
			</p>
			<p>
				Name
			</p>
			<p>
				Age
			</p>
			<p>
				Request Type
			</p>
			<p>
				From
			</p>
			<p>
				To
			</p>
			<p>
				Fee
			</p>
		</div>
	)
}

const Row = (props: RowProps) => {
	return (
		<div className='completed-scr-table_row'>
			<Link
				to={`/completed-scr/${props.hash}`}
				className='front-page-latest-link'
			>
				{truncate(props.hash)}
			</Link>
			<div>
				<CompletedSCRStatusComp status={props.status} />
			</div>
			<Link to={`/state/${props.stateNum}`} className='front-page-latest-link'>
				{`#${props.stateNum}`}
			</Link>
			<div>
				<NameDisplay name={props.name} maxLength={12} />
			</div>
			<p>
				{props.age}
			</p>
			<p>
				{props.requestType}
			</p>
			<div className='completed-scr-table_row_from'>
				<Link to={`/address/${props.from}`} className='front-page-latest-link'>
					{truncate(props.from)}
				</Link>
				<ArrowRight />
			</div>
			<Link to={`/address/${props.to}`} className='front-page-latest-link'>
				{truncate(props.to)}
			</Link>
			<p>
				{props.fee} Unit
			</p>
		</div>
	)
}

const LoadingRow = () => {
	return (
		<div className='completed-scr-table_row__loading'>
			<div>
				<Skeleton width={95} height={14} rx={4} />
			</div>
			<div>
				<Skeleton width={65} height={14} rx={4} />
			</div>
			<div>
				<Skeleton width={71} height={14} rx={4} />
			</div>
			<div>
				<Skeleton width={120} height={14} rx={4} />
			</div>
			<div>
				<Skeleton width={93} height={14} rx={4} />
			</div>
			<div>
				<Skeleton width={109} height={14} rx={4} />
			</div>
			<div>
				<Skeleton width={91} height={14} rx={4} />
			</div>
			<div>
				<Skeleton width={91} height={14} rx={4} />
			</div>
			<div>
				<Skeleton width={110} height={14} rx={4} />
			</div>
		</div>
	)
}

const Loading = memo(() => {
	return (
		<>
			{new Array(25).fill(1).map((_, idx) => {
				return <LoadingRow key={idx} />
			})}
		</>
	)
})

const parsePage = (p: string | null) => {
	if (!p) {
		return 1
	}
	const parsed = parseInt(p)
	if (isNaN(parsed)) {
		return 1
	}
	return parsed
}

export const CompletedSCRTable = () => {
	const [params, setParams] = useSearchParams()
	const currentPage = parsePage(params.get('page'))
	const { data } = useCompletedSCR(currentPage)
	const [totalPages, setTotalPages] = useState(1)

	useEffect(() => {
		if (data?.totalPages) {
			setTotalPages(data.totalPages)
		}
	}, [data])

	return (
		<div className='completed-scr-table'>
			<Header />
			{data
				? data?.currentList?.map(item => <Row key={item.hash} {...item} />)
				: <Loading />}
			<div className='paginator-container'>
				<Paginator
					currentPage={currentPage}
					totalPages={totalPages}
					onPageChange={page => {
						setParams(prev => {
							const newParams = new URLSearchParams(prev)
							newParams.set('page', page.toString())
							return newParams
						})
					}}
				/>
			</div>
		</div>
	)
}
