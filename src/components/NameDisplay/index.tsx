import * as Tooltip from '@radix-ui/react-tooltip'
import { type CSSProperties } from 'react'

import './index.scss'

export type NameDisplayProps = {
	name: string
	maxLength: number
	className?: string
	style?: CSSProperties
}

export const NameDisplay = (props: NameDisplayProps) => {
	// const [open, setOpen] = useState(false)

	// const handleOpen = (op: boolean) => {
	//   if(op) {
	//     setOpen(false)
	//   }else{
	//     if(props.name.length <= props.maxLength) {
	//       setOpen(true)
	//     }
	//   }
	// }

	return (
		<Tooltip.Root delayDuration={0}>
			<Tooltip.Trigger asChild>
				<p className={`name-display ${props.className ?? ''}`}>
					{props.name}
				</p>
			</Tooltip.Trigger>
			<Tooltip.Portal>
				<Tooltip.Content className='TooltipContent' sideOffset={5}>
					{props.name}
					<Tooltip.Arrow className='TooltipArrow' />
				</Tooltip.Content>
			</Tooltip.Portal>
		</Tooltip.Root>
	)
}
