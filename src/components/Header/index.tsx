import { Link } from 'react-router-dom'
import { ReactComponent as Icon } from '../../assets/icon-header.svg'
import { Switch } from './Switch'
import './index.scss'

const Header = () => {
	return (
		<header className='header'>
			<Link to='/'>
				<Icon />
			</Link>
			<Switch />
		</header>
	)
}

export { Header }
