import { ReactComponent as Moon } from '../../assets/moon-enabled.svg'
import { ReactComponent as Sun } from '../../assets/sun-enabled.svg'
// import { useState } from 'react'
import './Switch.scss'
import { useTheme, useToggleTheme } from '../../hooks/use-theme'
// import { ThemeStorageKey, switchToDark, switchToLight } from '../../utils/theme'

const Switch = () => {
	// const [theme, setTheme] = useState(() => {
	//   return localStorage.getItem(ThemeStorageKey) ?? 'light'
	// })

	// const toggleTheme = () => {
	//   const prev = theme

	//   setTheme(theme === 'light' ? 'dark' : 'light')

	//   prev === 'light' ? switchToDark() : switchToLight()
	// }

	const theme = useTheme()
	const toggleTheme = useToggleTheme()

	return (
		<button
			onClick={toggleTheme}
			className='theme-switch'
			type='button'
			aria-label='Toggle theme'
			role='switch'
		>
			{theme === 'light'
				? (
					<>
						<Sun />
					</>
				)
				: (
					<>
						<Moon />
					</>
				)}
		</button>
	)
}

export { Switch }
