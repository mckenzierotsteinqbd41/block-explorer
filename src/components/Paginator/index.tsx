import { ReactComponent as FirstIcon } from '../../assets/first-page.svg'
import { ReactComponent as LastIcon } from '../../assets/last-page.svg'
import { ReactComponent as NextIcon } from '../../assets/next-page.svg'
import { ReactComponent as PrevIcon } from '../../assets/prev-page.svg'
import './index.scss'

export type PaginatorProps = {
	currentPage: number
	totalPages: number
	onPageChange: (page: number) => void
}

const enum PageKind {
	Prev = 1,
	Next = 2,
	First = 3,
	Last = 4,
}

type ButtonProps = {
	kind: PageKind
	onClick: () => void
	disabled: boolean
}

const Button = (props: ButtonProps) => {
	const Icon = (() => {
		switch (props.kind) {
			case PageKind.Prev:
				return <PrevIcon />
			case PageKind.Next:
				return <NextIcon />
			case PageKind.First:
				return <FirstIcon />
			case PageKind.Last:
				return <LastIcon />
		}
	})()

	return (
		<button
			onClick={props.onClick}
			disabled={props.disabled}
			className='paginator-button'
		>
			{Icon}
		</button>
	)
}

export const Paginator = (props: PaginatorProps) => {
	return (
		<div className='paginator'>
			<Button
				kind={PageKind.First}
				onClick={() => props.onPageChange(1)}
				disabled={props.currentPage === 1}
			/>
			<Button
				kind={PageKind.Prev}
				onClick={() => props.onPageChange(props.currentPage - 1)}
				disabled={props.currentPage === 1}
			/>
			<p>
				{props.currentPage} / {props.totalPages}
			</p>
			<Button
				kind={PageKind.Next}
				onClick={() => props.onPageChange(props.currentPage + 1)}
				disabled={props.currentPage === props.totalPages}
			/>
			<Button
				kind={PageKind.Last}
				onClick={() => props.onPageChange(props.totalPages)}
				disabled={props.currentPage === props.totalPages}
			/>
		</div>
	)
}
