import { useStatesTotalAmount } from '../../../hooks/use-states-total-amount'
import { Footer } from '../../Footer'
import { Skeleton } from '../../Skeleton'
import { StatesTable } from '../Table'
import './index.scss'

const title = 'State'
const description = 'Total Amount: '

const formatAmount = (d: number) => {
	return new Intl.NumberFormat('en-US').format(d)
}

export const StatesMain = () => {
	const { data } = useStatesTotalAmount()

	return (
		<main className='main-completed-scr'>
			<section className='main-completed-scr-bg'>
				<section className='main-completed-scr-topper'>
					<h1>{title}</h1>
					{data
						? <p>{`${description}${formatAmount(data)}`}</p>
						: <Skeleton width={143} height={14} rx={4} />}
				</section>
				<StatesTable />
			</section>
			<Footer />
		</main>
	)
}
