import { Link, useSearchParams } from 'react-router-dom'
import './index.scss'
import { memo, useEffect, useState } from 'react'
import { useStates } from '../../../hooks/use-states'
import { Paginator } from '../../Paginator'
import { Skeleton } from '../../Skeleton'

type RowProps = {
	stateNum: string
	size: number
	age: string
	gasUsed: string
}

const Header = () => {
	return (
		<div className='completed-scr-table_header states-table_header'>
			<p>
				State Num
			</p>
			<p>
				Size
			</p>
			<p>
				Age
			</p>
			<p>
				Gas Used
			</p>
		</div>
	)
}

const Row = (props: RowProps) => {
	return (
		<div className='completed-scr-table_row states-table_row'>
			<Link to={`/state/${props.stateNum}`} className='front-page-latest-link'>
				{`#${props.stateNum}`}
			</Link>
			<Link to={`/state/${props.stateNum}`} className='front-page-latest-link'>
				{`#${props.size}`}
			</Link>
			<p>
				{props.age}
			</p>
			<p>
				{props.gasUsed} Unit
			</p>
		</div>
	)
}

const LoadingRow = () => {
	return (
		<div className='completed-scr-table_row__loading states-table_row__loading'>
			<div>
				<Skeleton width={100} height={14} rx={4} />
			</div>
			<div>
				<Skeleton width={100} height={14} rx={4} />
			</div>
			<div>
				<Skeleton width={100} height={14} rx={4} />
			</div>
			<div>
				<Skeleton width={160} height={14} rx={4} />
			</div>
		</div>
	)
}

const Loading = memo(() => {
	return (
		<>
			{new Array(25).fill(1).map((_, idx) => {
				return <LoadingRow key={idx} />
			})}
		</>
	)
})

const parsePage = (p: string | null) => {
	if (!p) {
		return 1
	}
	const parsed = parseInt(p)
	if (isNaN(parsed)) {
		return 1
	}
	return parsed
}

export const StatesTable = () => {
	const [params, setParams] = useSearchParams()
	const currentPage = parsePage(params.get('page'))
	const { data } = useStates(currentPage)
	const [totalPages, setTotalPages] = useState(1)

	useEffect(() => {
		if (data?.totalPages) {
			setTotalPages(data.totalPages)
		}
	}, [data])

	return (
		<div className='completed-scr-table'>
			<Header />
			{data
				? data?.currentList?.map(item => <Row key={item.stateNum} {...item} />)
				: <Loading />}
			<div className='paginator-container'>
				<Paginator
					currentPage={currentPage}
					totalPages={totalPages}
					onPageChange={page => {
						setParams(prev => {
							const newParams = new URLSearchParams(prev)
							newParams.set('page', page.toString())
							return newParams
						})
					}}
				/>
			</div>
		</div>
	)
}
