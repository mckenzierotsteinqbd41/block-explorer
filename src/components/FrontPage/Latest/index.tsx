import { ViewAllButton } from '../../ViewAllButton'
import './index.scss'
import { Link } from 'react-router-dom'
import { ReactComponent as BoxIcon } from '../../../assets/box.svg'
import { ReactComponent as ListIcon } from '../../../assets/list.svg'
import { useLatest } from '../../../hooks/use-latest'
import { CompletedSCRStatus } from '../../../types/completed-scr'
import { truncate } from '../../../utils/truncate'
import { CompletedSCRStatusComp } from '../../CompletedSCRStatus'
import { Skeleton } from '../../Skeleton'

type LastestStateItemProps = {
	stateNum: string
	gasUsed: string
	age: string
}

type LatestCompletedSCRItemProps = {
	hash: string
	status: CompletedSCRStatus
	from: string
	age: string
}

const LatestStateItemLoading = () => {
	return (
		<div className='front-page-latest-states-item'>
			<BoxIcon />
			<Skeleton width={124} height={14} rx={4} />
			<Skeleton width={190} height={14} rx={4} />
			<Skeleton width={130} height={14} rx={4} />
		</div>
	)
}

const LatestCompletedSCRItemLoading = () => {
	return (
		<div className='front-page-latest-completed-scr-item'>
			<ListIcon />
			<Skeleton width={121} height={14} rx={4} />
			<Skeleton width={87} height={14} rx={4} />
			<Skeleton width={124} height={14} rx={4} />
			<Skeleton width={88} height={14} rx={4} />
		</div>
	)
}

const LatestStateItem = ({ gasUsed, stateNum, age }: LastestStateItemProps) => {
	return (
		<div className='front-page-latest-states-item'>
			<BoxIcon />
			<Link to={`/state/${stateNum}`} className='front-page-latest-link'>
				{`#${stateNum}`}
			</Link>
			<p>{`${gasUsed} Unit`}</p>
			<p>{age}</p>
		</div>
	)
}

const LatestCompletedSCRItem = (
	{ hash, status, from, age }: LatestCompletedSCRItemProps,
) => {
	return (
		<div className='front-page-latest-completed-scr-item'>
			<ListIcon />
			<Link
				className='front-page-latest-completed-scr-item-hash front-page-latest-link'
				to={`/completed-scr/${hash}`}
			>
				{truncate(hash)}
			</Link>
			<div className='front-page-latest-completed-scr-item-status'>
				<CompletedSCRStatusComp status={status} />
			</div>
			<Link
				className='front-page-latest-completed-scr-item-from front-page-latest-link'
				to={`/address/${from}`}
			>
				{truncate(from)}
			</Link>
			<div className='front-page-latest-completed-scr-item-age'>
				{age}
			</div>
		</div>
	)
}

const LatestStates = () => {
	const { data } = useLatest()

	return (
		<div className='front-page-latest-states'>
			<h2 className='front-page-latest-title'>
				Lastest states
			</h2>
			<div className='front-page-latest-states-content front-page-latest-content'>
				<div className='front-page-latest-content_header front-page-latest-states-content_header'>
					<p>
						State Num
					</p>
					<p>
						Gas Used
					</p>
					<p>
						Age
					</p>
				</div>
				<div className='front-page-latest-states-content_body'>
					{data
						? (
							data.latestStates.map((item) => (
								<LatestStateItem
									key={item.stateNum}
									gasUsed={item.gasUsed}
									stateNum={item.stateNum}
									age={item.age}
								/>
							))
						)
						: (
							new Array(6).fill(0).map((_, idx) => (
								<LatestStateItemLoading key={idx} />
							))
						)}
				</div>
				<ViewAllButton text='View all' to='/states' />
			</div>
		</div>
	)
}

/**
 * Lastest completed state change request
 */
const LatestCompletedSCR = () => {
	const { data } = useLatest()

	return (
		<div className='front-page-latest-completed-scr'>
			<h2 className='front-page-latest-title'>
				Lastest completed state change request
			</h2>
			<div className='front-page-latest-completed-scr-content front-page-latest-content'>
				<div className='front-page-latest-content_header front-page-latest-completed-scr-content_header'>
					<p>
						Hash
					</p>
					<p>
						Status
					</p>
					<p>
						From
					</p>
					<p>
						Age
					</p>
				</div>
				<div className='front-page-latest-completed-scr-content_body'>
					{data
						? (
							data.latestCompletedSCR.map((item) => (
								<LatestCompletedSCRItem
									key={item.hash}
									hash={item.hash}
									status={item.status}
									from={item.from}
									age={item.age}
								/>
							))
						)
						: (
							new Array(6).fill(0).map((_, idx) => (
								<LatestCompletedSCRItemLoading key={idx} />
							))
						)}
				</div>
				<ViewAllButton text='View all' to='/completed-scr' />
			</div>
		</div>
	)
}

const Latest = () => {
	return (
		<div className='front-page-latest'>
			<LatestStates />
			<LatestCompletedSCR />
		</div>
	)
}

export { Latest }
