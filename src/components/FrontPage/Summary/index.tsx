import * as Separator from '@radix-ui/react-separator'
import { ReactComponent as Clock } from '../../../assets/clock.svg'
import { ReactComponent as Coin } from '../../../assets/dollar-coin.svg'
import { ReactComponent as Earth } from '../../../assets/earth.svg'
import { ReactComponent as Person } from '../../../assets/person.svg'
import './index.scss'
import type { ReactNode } from 'react'
import { useSummary } from '../../../hooks/use-summary'
import { Skeleton } from '../../Skeleton'

const SummaryLoading = <Skeleton width={303} height={20} rx={4} />

type ItemProps = {
	icon: ReactNode
	title: string
	value: ReactNode
	equalsTo?: string
}

const Item = (props: ItemProps) => {
	const { icon, title, value, equalsTo } = props

	return (
		<div className='front-page-summary-item'>
			{icon}
			<div className='front-page-summary-item-content'>
				<div className='front-page-summary-item-title'>{title}</div>
				<div className='front-page-summary-item-value'>
					{value}
					{equalsTo
						? (
							<span className='front-page-summary-item-equals-to'>
								{`(${equalsTo})`}
							</span>
						)
						: null}
				</div>
			</div>
		</div>
	)
}

const Loading = () => {
	return (
		<div className='front-page-summary'>
			<div className='front-page-summary-column'>
				<Item
					icon={<Earth />}
					title='Total State Transition request'
					value={SummaryLoading}
				/>
				<Separator.Root className='SeparatorRoot' />
				<Item
					icon={<Clock />}
					title='24h State Transition request'
					value={SummaryLoading}
				/>
			</div>
			<Separator.Root orientation='vertical' className='SeparatorRoot' />
			<div className='front-page-summary-column'>
				<Item icon={<Coin />} title='Med Gas Price' value={SummaryLoading} />
				<Separator.Root className='SeparatorRoot' />
				<Item
					icon={<Person />}
					title='Active Interactor'
					value={SummaryLoading}
				/>
			</div>
			<Separator.Root orientation='vertical' className='SeparatorRoot' />
			<div className='front-page-summary-column'>
			</div>
		</div>
	)
}

const Summary = () => {
	const { data } = useSummary()

	if (!data) {
		return <Loading />
	}

	const {
		totalStateTransitionRequest,
		twentyFourHoursStateTransitionRequest,
		medGasPrice,
		activeInteractor,
	} = data

	const formattedTotalState = new Intl.NumberFormat('en-US').format(
		totalStateTransitionRequest,
	)
	const formatted24hours = new Intl.NumberFormat('en-US').format(
		twentyFourHoursStateTransitionRequest,
	)
	const formattedMedGasPrice = new Intl.NumberFormat('en-US', {
		minimumSignificantDigits: 5,
	}).format(medGasPrice)
	const formattedActiveInteractor = new Intl.NumberFormat('en-US').format(
		activeInteractor,
	)

	return (
		<div className='front-page-summary'>
			<div className='front-page-summary-column'>
				<Item
					icon={<Earth />}
					title='Total State Transition request'
					value={formattedTotalState}
				/>
				<Separator.Root className='SeparatorRoot' />
				<Item
					icon={<Clock />}
					title='24h State Transition request'
					value={formatted24hours}
					equalsTo='3.2TPS'
				/>
			</div>
			<Separator.Root orientation='vertical' className='SeparatorRoot' />
			<div className='front-page-summary-column'>
				<Item
					icon={<Coin />}
					title='Med Gas Price'
					value={`${formattedMedGasPrice} Unit`}
					equalsTo='$0.002'
				/>
				<Separator.Root className='SeparatorRoot' />
				<Item
					icon={<Person />}
					title='Active Interactor'
					value={formattedActiveInteractor}
				/>
			</div>
			<Separator.Root orientation='vertical' className='SeparatorRoot' />
			<div className='front-page-summary-column'>
			</div>
		</div>
	)
}

export { Summary }
