import { Latest } from '../Latest'
import { Summary } from '../Summary'
import './index.scss'

const title = 'Reach Block-State Explorer'
const description =
	'REACH Block Explorer provides all the information to deep dive into transactions, blocks, contracts, and much more.'

const Main = () => {
	return (
		<main className='main-frontpage' role='main'>
			<section className='main-frontpage-topper'>
				<h1>{title}</h1>
				<p>{description}</p>
			</section>
			<section className='main-frontpage-bg'></section>
			<Summary />
			<Latest />
		</main>
	)
}

export { Main }
