import cn from 'classnames'
import { CompletedSCRStatus } from '../../types/completed-scr'
import './index.scss'

export type CompletedSCRStatusProps = {
	status: CompletedSCRStatus
}

const CompletedSCRStatusComp = ({ status }: CompletedSCRStatusProps) => {
	return (
		<div
			className={cn(
				'completed-scr-status',
				{
					'completed-scr-status__success':
						status === CompletedSCRStatus.Success,
					'completed-scr-status__failed': status === CompletedSCRStatus.Failed,
				},
			)}
		>
			{status}
		</div>
	)
}

export { CompletedSCRStatusComp }
