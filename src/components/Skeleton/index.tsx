import ContentLoader from 'react-content-loader'

export type SkeletonProps = {
	width: number
	height: number
	rx: number
}

export const Skeleton = (props: SkeletonProps) => {
	const { width, height, rx } = props

	return (
		<ContentLoader
			speed={0.6}
			width={width}
			height={height}
			viewBox={`0 0 ${width} ${height}`}
			backgroundColor='var(--skeleton-bg)'
			foregroundColor='var(--skeleton-fg)'
		>
			<rect x='0' y='0' rx={rx} ry={rx} width={width} height={height} />
		</ContentLoader>
	)
}
