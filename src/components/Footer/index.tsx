import { ReactComponent as Icon } from '../../assets/icon-header.svg'
import { Link1104 } from '../Link1104'
import './index.scss'

const copyright = '© 2023 Reach. All rights reserved.'

const Footer = () => {
	return (
		<footer className='footer'>
			<div className='footer-copyright'>
				<Icon />
				<p>{copyright}</p>
			</div>
			<div className='footer-links'>
				<Link1104 text='Terms of Service' href='/terms' />
				<Link1104 text='Network Status' href='/network' />
			</div>
		</footer>
	)
}

export { Footer }
