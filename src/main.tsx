import * as Tooltip from '@radix-ui/react-tooltip'
import React from 'react'
import ReactDOM from 'react-dom/client'
import { RouterProvider } from 'react-router-dom'
import { router } from './App'
import 'reset-css'
import './index.css'
import { SWRConfig } from 'swr'
import { ThemeProvider } from './hooks/use-theme'

ReactDOM.createRoot(document.getElementById('root')!).render(
	<React.StrictMode>
		<SWRConfig
			value={{
				revalidateOnFocus: false,
			}}
		>
			<ThemeProvider>
				<Tooltip.Provider>
					<RouterProvider router={router} />
				</Tooltip.Provider>
			</ThemeProvider>
		</SWRConfig>
	</React.StrictMode>,
)
