import { createBrowserRouter } from 'react-router-dom'
import './App.css'
import { PageCompletedSCR } from './pages/completed-scr'
import { GlobalErrorPage } from './pages/error'
import { Root } from './pages/Root'
import { PageStates } from './pages/states'

const router = createBrowserRouter([
	{
		path: '/',
		element: <Root />,
		errorElement: <GlobalErrorPage />,
	},
	{
		path: '/completed-scr',
		element: <PageCompletedSCR />,
	},
	{
		path: '/states',
		element: <PageStates />,
	},
])

export { router }
