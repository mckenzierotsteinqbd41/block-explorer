const ThemeStorageKey = 'reach-explorer.theme'

const htmlEl = document.querySelector('html')

const switchToDark = () => {
	htmlEl?.classList.add('dark')
	localStorage.setItem(ThemeStorageKey, 'dark')
}

const switchToLight = () => {
	htmlEl?.classList.remove('dark')
	localStorage.setItem(ThemeStorageKey, 'light')
}

const setThemeBeforeRender = () => {
	const theme = localStorage.getItem(ThemeStorageKey)
	if (theme === 'dark') {
		htmlEl?.classList.add('dark')
	}

	return theme ?? 'light'
}

export { setThemeBeforeRender, switchToDark, switchToLight, ThemeStorageKey }
