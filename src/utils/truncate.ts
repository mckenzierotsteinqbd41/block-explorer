/**
 * 用于截断字符串，并添加省略号
 * @param str
 * @param prefixLength
 * @param suffixLength
 * @returns
 */
const truncate = (str: string, prefixLength = 6, suffixLength = 4) => {
	const prefix = str.slice(0, prefixLength)
	const suffix = str.slice(-suffixLength)

	return `${prefix}...${suffix}`
}

export { truncate }
