import useSWR from 'swr'
import { buildFetcher } from './fetcher'

export type SummaryData = {
	totalStateTransitionRequest: bigint
	twentyFourHoursStateTransitionRequest: bigint
	medGasPrice: number
	activeInteractor: bigint
}

const fetcher = buildFetcher<SummaryData>({
	totalStateTransitionRequest: 104924n,
	twentyFourHoursStateTransitionRequest: 2093n,
	medGasPrice: 0.036546,
	activeInteractor: 154584n,
})

const useSummary = () => {
	const result = useSWR('/api/summary', fetcher, {
		dedupingInterval: 5 * 1000,
		revalidateOnFocus: false,
	})

	return result
}

export { useSummary }
