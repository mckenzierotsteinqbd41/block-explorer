import useSWR from 'swr'
import { buildFetcher } from './fetcher'

const fetcher = buildFetcher<number>(Math.floor(Math.random() * 5000))

export const useSCRTotalAmount = () => {
	const result = useSWR('/api/scr-amount', fetcher, {
		revalidateOnFocus: true,
		revalidateOnReconnect: true,
		refreshInterval: 10 * 1000,
	})

	return result
}
