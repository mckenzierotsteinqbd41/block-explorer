import useSWR from 'swr'
import { CompletedSCRStatus } from '../types/completed-scr'
import { buildFetcher } from './fetcher'

type LatestState = {
	stateNum: string
	gasUsed: string
	age: string
}

type LatestCompletedSCR = {
	hash: string
	status: CompletedSCRStatus
	from: string
	age: string
}

export type Latest = {
	latestStates: LatestState[]
	latestCompletedSCR: LatestCompletedSCR[]
}

const fetcher = buildFetcher<Latest>({
	latestStates: new Array(6).fill(null).map((_, i) => ({
		stateNum: `417${i}`,
		gasUsed: (Math.random() / 1000).toString().substring(0, 10),
		age: `5 mins ago`,
	})),
	latestCompletedSCR: new Array(4).fill(null).map((_, i) => ({
		hash: `0x58weabc4321ab${i}`,
		status: CompletedSCRStatus.Processing,
		from: `0x3bea2343243243`,
		age: `5 mins ago`,
	})).concat([
		{
			hash: `0x58weabc4321ab4`,
			status: CompletedSCRStatus.Success,
			from: `0x3bea2343243243`,
			age: `5 mins ago`,
		},
		{
			hash: `0x58weabc4321ab5`,
			status: CompletedSCRStatus.Failed,
			from: `0x3bea2343243243`,
			age: `5 mins ago`,
		},
	]),
})

export const useLatest = () => {
	const result = useSWR('/api/latest', fetcher, {
		revalidateOnFocus: true,
		revalidateOnReconnect: true,
		refreshInterval: 10 * 1000,
	})

	return result
}
