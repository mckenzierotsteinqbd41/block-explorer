import useSWR from 'swr'
import { buildFetcher } from './fetcher'

const fetcher = buildFetcher<number>(Math.floor(Math.random() * 5000))

export const useStatesTotalAmount = () => {
	const result = useSWR('/api/states-amount', fetcher, {
		revalidateOnFocus: true,
		revalidateOnReconnect: true,
		refreshInterval: 10 * 1000,
	})

	return result
}
