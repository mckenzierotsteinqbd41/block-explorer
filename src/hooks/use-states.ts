import useSWR from 'swr'
import { buildFetcher } from './fetcher'

type State = {
	stateNum: string
	size: number
	age: string
	gasUsed: string
}

const fetcher = buildFetcher<{
	currentList: State[]
	totalPages: number
}>({
	currentList: new Array(25).fill(null).map((_, i) => ({
		age: '5 mins ago',
		size: Math.floor(Math.random() * 100),
		gasUsed: '0.00315458',
		stateNum: `33${i}`,
	})).concat([
		{
			age: '5 mins ago',
			size: Math.floor(Math.random() * 100),
			gasUsed: '0.00315458',
			stateNum: `44`,
		},
		{
			age: '5 mins ago',
			size: Math.floor(Math.random() * 100),
			gasUsed: '0.00315458',
			stateNum: '45',
		},
	]),
	totalPages: Math.floor(Math.random() * 100),
})

/**
 * @param page 从 1 开始计数
 * @returns
 */
export const useStates = (page: number) => {
	const result = useSWR(`/api/states?page=${page}`, fetcher, {
		revalidateOnFocus: true,
		revalidateOnReconnect: true,
		refreshInterval: 10 * 1000,
	})

	return result
}
