import { createContext, useContext, useState } from 'react'
import {
	setThemeBeforeRender,
	switchToDark,
	switchToLight,
} from '../utils/theme'

const themeFromStorage = setThemeBeforeRender()

const ThemeContext = createContext(themeFromStorage)
const ToggleThemeContext = createContext(() => {})

const ThemeProvider = ({ children }: { children: React.ReactNode }) => {
	const [theme, setTheme] = useState(themeFromStorage)

	const toggleTheme = () => {
		const prev = theme

		setTheme(prev => (prev === 'light' ? 'dark' : 'light'))

		prev === 'light' ? switchToDark() : switchToLight()
	}

	return (
		<ThemeContext.Provider value={theme}>
			<ToggleThemeContext.Provider value={toggleTheme}>
				{children}
			</ToggleThemeContext.Provider>
		</ThemeContext.Provider>
	)
}

const useTheme = () => useContext(ThemeContext)
const useToggleTheme = () => useContext(ToggleThemeContext)

export { ThemeProvider, useTheme, useToggleTheme }
