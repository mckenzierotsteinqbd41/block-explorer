const buildFetcher = <T>(mockData: T, delay = 500) => {
	if (import.meta.env.MODE !== 'production') {
		return () =>
			new Promise<T>((res) => {
				setTimeout(() => res(mockData), delay)
			})
	}

	return (url: string) => fetch(url).then(res => res.json() as T)
}

export { buildFetcher }
