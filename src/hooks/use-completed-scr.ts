import useSWR from 'swr'
import { CompletedSCRStatus } from '../types/completed-scr'
import { buildFetcher } from './fetcher'

type LatestCompletedSCR = {
	hash: string
	status: CompletedSCRStatus
	stateNum: string
	name: string
	age: string
	requestType: string
	from: string
	to: string
	fee: string
}

const fetcher = buildFetcher<{
	currentList: LatestCompletedSCR[]
	totalPages: number
}>({
	currentList: new Array(25).fill(null).map((_, i) => ({
		hash: `0x58weabc4321ab${i}`,
		status: CompletedSCRStatus.Processing,
		from: `0x3bea2343243243`,
		age: `5 mins ago`,
		name: 'transfer',
		requestType: 'State Creation',
		to: '0x3432434',
		fee: '0.00315458',
		stateNum: '33',
	})).concat([
		{
			hash: `0x58weabc4321ab49`,
			status: CompletedSCRStatus.Success,
			from: `0x3bea2343243243`,
			age: `5 mins ago`,
			name: 'transfer',
			requestType: 'Publish',
			to: '0x3432434',
			fee: '0.00315458',
			stateNum: '33',
		},
		{
			hash: `0x58weabc4321ab59`,
			status: CompletedSCRStatus.Failed,
			from: `0x3bea2343243243`,
			age: `5 mins ago`,
			name: 'Loan Liquidate Bsdankfna',
			requestType: 'Publish',
			to: '0x3432434',
			fee: '0.00315458',
			stateNum: '33',
		},
	]),
	totalPages: Math.floor(Math.random() * 100),
})

/**
 * @param page 从 1 开始计数
 * @returns
 */
export const useCompletedSCR = (page: number) => {
	const result = useSWR(`/api/completed-scr?page=${page}`, fetcher, {
		revalidateOnFocus: true,
		revalidateOnReconnect: true,
		refreshInterval: 10 * 1000,
	})

	return result
}
