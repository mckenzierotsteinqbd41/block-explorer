/**
 * Completed State Change Request Status
 */
export enum CompletedSCRStatus {
	Processing = 'Processing',
	Prepared = 'Prepared',
	Packed = 'Packed',
	Committed = 'Committed',
	Success = 'Success',
	Failed = 'Failed',
}
