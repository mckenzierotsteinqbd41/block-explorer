import react from '@vitejs/plugin-react'
import { visualizer } from 'rollup-plugin-visualizer'
import { defineConfig } from 'vite'
import { ViteImageOptimizer } from 'vite-plugin-image-optimizer'
import svgr from 'vite-plugin-svgr'

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
	return {
		plugins: [
			react(),
			svgr(),
			ViteImageOptimizer({
				logStats: false,
				png: {
					quality: 100,
				},
				jpeg: {
					quality: 75,
				},
				jpg: {
					quality: 75,
				},
			}),
			mode === 'debug' && visualizer(),
		],
	}
})
